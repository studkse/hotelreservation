 package hotelreservation.controller;



import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hotelreservation.domain.Hotel;
import hotelreservation.service.HotelService;

// https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/ResponseStatus.html
// https://www.baeldung.com/spring-response-status
// https://www.baeldung.com/spring-response-entity

@RestController
@RequestMapping("/hotels")
@CrossOrigin(origins = "http://localhost:3000")
public class HotelController { 
    private HotelService hotelService;

    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }
    
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @GetMapping("/teapot")
    public void teaPot() {

    }
    
    // https://spring.io/guides/gs/rest-service-cors/
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Hotel> getAllHotels() {
        return hotelService.findAllHotels();
    }
    
    @GetMapping("/allresponses")
    @Produces({ "application/json" })
    public Response getAllHotelsResponse() {
		return Response.status(200)
				.entity(hotelService.findAllHotels())
				.build();
	}
    
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Hotel getHotelById(@PathVariable Long id) {
        return hotelService.findHotelById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Hotel createHotel(@Valid @RequestBody Hotel hotel) {
        return hotelService.createHotel(hotel);
    }
    
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Hotel changeHOtelById(@RequestBody Hotel newHotel, @PathVariable Long id) {
        return hotelService.changeHotelById(newHotel, id);
    }
    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void removeHotelById(@PathVariable Long id) {
        hotelService.removeHotelById(id);
    }
    
    
    

}
