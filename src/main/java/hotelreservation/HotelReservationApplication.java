package hotelreservation;

import java.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import hotelreservation.domain.*;
import hotelreservation.repository.HotelRepository;

@SpringBootApplication
public class HotelReservationApplication {
	private static final Logger log = LoggerFactory.getLogger(HotelReservationApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(HotelReservationApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(HotelRepository repository) {
		return (args) -> {
			Hotel h1 = new Hotel();
			try {
				// save hotel(s)
				h1 = new Hotel();
				h1.setFantasyName("Hotel Valora");
				h1.setAdress_IDFK(
						new Adress("Karl-Liebknecht-Strasse", 18, "Brockel", "Niedersachsen", 27386, "Deutschland"));
				repository.save(h1);
			} catch (Exception e) {
				System.out.println("----------------------------"
						+ "\nhotel NICHT hinzugefuegt\n"
						+ "--------------------------------");
				e.printStackTrace();
				
			}
			// add customers
			Customer c1 = new Customer();
			c1.setFname("Thomas");
			c1.setLname("Feud");
			c1.setEmail("t.feud@mail.com");
			c1.setPw("1234");
			c1.setTel("0761234567");
			c1.setAdress_IDFK(new Adress("Via Albarelle", 99, "St-Oyens", "", 1187, "Schweiz"));
			c1.setHotel_IDFK(h1);
			Customer c2 = new Customer();
			c2.setFname("Maja");
			c2.setLname("Mueller");
			c2.setEmail("m.,mueller@mail.com");
			c2.setPw("1234");
			c2.setTel("0761294567");
			c2.setAdress_IDFK(new Adress("Zürichstrasse ", 58, "Bachenbülach", "", 8184, "Schweiz"));
			c2.setHotel_IDFK(h1);
			h1.getCustomers().add(c1);
			h1.getCustomers().add(c2);
			// add workers
			Worker w1 = new Worker();
			w1.setFname("Ueli");
			w1.setLname("Heinz");
			w1.setEmail("u.heinz@mail.com");
			w1.setPw("4321");
			w1.setTel("0435671234");
			w1.setAdress_IDFK(new Adress("Lichtmattstrasse", 185, "Burtigny", "", 1268, "Schweiz"));
			w1.setHotel_IDFK(h1);
			Worker w2 = new Worker();
			w2.setFname("Leonie");
			w2.setLname("Busque");
			w2.setEmail("l.busque@mail.com");
			w2.setPw("4321");
			w2.setTel("0435017464");
			w2.setAdress_IDFK(new Adress("Untere Bahnhofstrasse", 100, "Burtigny", "Casserio", 6722, "Schweiz"));
			w2.setHotel_IDFK(h1);
			h1.getWorkers().add(w1);
			h1.getWorkers().add(w2);
			// add rooms
			Room r1 = new Room("single", 233.00);
			Room r2 = new Room("triple", 450.00);
			Room r3 = new Room("queen", 500.00);
			Room r4 = new Room("studio", 200.00);
			Room r5 = new Room("double", 333.00);
			Room r6 = new Room("quad", 650.00);
			Room r7 = new Room("king", 500.00);
			Room r8 = new Room("studio", 200.00);
			h1.getRooms().add(r1);
			h1.getRooms().add(r2);
			h1.getRooms().add(r3);
			h1.getRooms().add(r4);
			h1.getRooms().add(r5);
			h1.getRooms().add(r6);
			h1.getRooms().add(r7);
			h1.getRooms().add(r8);
			// create reservation(s)
			Reservation re1 = new Reservation();
			re1.setDateFrom(LocalDate.of(2017, 1, 13));
			re1.setDateTo(LocalDate.of(2017, 1, 18));
			re1.setRoom_IDFK(r1);
			re1.setCustomer_IDFK(c1);

			repository.save(h1);

			// fetch all hotels
			log.info("Hotels found with findAll():");
			log.info("-------------------------------");
			for (Hotel hotel : repository.findAll()) {
				log.info(hotel.toString());
			}
			log.info("");

//			// fetch an individual customer by ID
//			repository.findById(1).ifPresent(customer -> {
//				log.info("Customer found with findById(1L):");
//				log.info("--------------------------------");
//				log.info(customer.toString());
//				log.info("");
//			});
//
//			// fetch customers by last name
//			log.info("Customer found with findByLastName('Bauer'):");
//			log.info("--------------------------------------------");
//			repository.findByLastname("Bauer").forEach(bauer -> {
//				log.info(bauer.toString());
//			});
//			// for (Customer bauer : repository.findByLastName("Bauer")) {
//			// log.info(bauer.toString());
//			// }
//			log.info("customers");

		};
	}
}
