package hotelreservation.domain;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reservation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long reservation_IDPK;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	@ManyToOne
	@JoinColumn(name = "room_IDFK", referencedColumnName = "room_IDPK")
	private Room room_IDFK;
	@ManyToOne
	@JoinColumn(name = "customer_IDFK", referencedColumnName = "customer_IDPK")
	private Customer customer_IDFK;
	/* Constructors */
	public Reservation() {
		super();
		// TODO Auto-generated constructor stub
	}
	/* Getters and Setters */
	public Long getReservation_IDPK() {
		return reservation_IDPK;
	}
	public void setReservation_IDPK(Long reservation_IDPK) {
		this.reservation_IDPK = reservation_IDPK;
	}
	
	public LocalDate getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}
	public LocalDate getDateTo() {
		return dateTo;
	}
	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}
	public Room getRoom_IDFK() {
		return room_IDFK;
	}
	public void setRoom_IDFK(Room room_IDFK) {
		this.room_IDFK = room_IDFK;
	}
	public Customer getCustomer_IDFK() {
		return customer_IDFK;
	}
	public void setCustomer_IDFK(Customer customer_IDFK) {
		this.customer_IDFK = customer_IDFK;
	}
	/* Methods (Additional) */
	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd");
		return "Reservation [reservation_IDPK=" + reservation_IDPK + ", dateFrom=" + dateFrom.format(formatter) + ", dateTo=" + dateTo.format(formatter)
				+ "]";
	}
	public double giveCost() {
		
		return 1.0;
	}
}
