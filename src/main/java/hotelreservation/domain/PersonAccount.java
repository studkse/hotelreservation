package hotelreservation.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass 
abstract class PersonAccount {

	private String fname;
	private String lname;
	private String email;
	private String pw;
	private String tel;
	@ManyToOne
	@JoinColumn(name = "adress_IDFK", referencedColumnName = "adress_IDPK")
	private Adress adress_IDFK;
	/*Constructors */
	public PersonAccount() {
		super();
		// TODO Auto-generated constructor stub
	}
	/* Getters and Setters */
	public String getFname() {
		return fname;
	}
	
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Adress getAdress_IDFK() {
		return adress_IDFK;
	}
	public void setAdress_IDFK(Adress adress_IDFK) {
		this.adress_IDFK = adress_IDFK;
	}
	/* Methods (Additional) */
}
