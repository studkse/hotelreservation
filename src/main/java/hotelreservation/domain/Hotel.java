package hotelreservation.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

@Entity
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long hotel_IDPK;
	@Size(max = 64)
	@Column(nullable = false)
	private String fantasyName;
	@OneToOne
	@JoinColumn(name = "adress_IDFK", referencedColumnName = "adress_IDPK")
	private Adress adress_IDFK;
	@OneToMany(mappedBy="customer_IDPK")
	private List<Customer> customers = new ArrayList<>();
	@OneToMany(mappedBy="worker_IDPK")
	private List<Worker> workers = new ArrayList<>();
	@OneToMany(mappedBy="room_IDPK")
	private List<Room> rooms = new ArrayList<>();
	
	/* Constructors */
	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Hotel(String fantasyName) {
		super();
		this.fantasyName = fantasyName;
	}

	/* Getters and Setters */
	public Long getHotel_IDPK() {
		return hotel_IDPK;
	}
	public void setHotel_IDPK(Long hotel_IDPK) {
		this.hotel_IDPK = hotel_IDPK;
	}
	public String getFantasyName() {
		return fantasyName;
	}
	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}
	public Adress getAdress_IDFK() {
		return adress_IDFK;
	}
	public void setAdress_IDFK(Adress adress_IDFK) {
		this.adress_IDFK = adress_IDFK;
	}
	public List<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
	public List<Worker> getWorkers() {
		return workers;
	}
	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}
	public List<Room> getRooms() {
		return rooms;
	}
	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}
	/* Methods (Additional) */
	@Override
	public String toString() {
		return "Hotel [hotel_IDPK=" + hotel_IDPK + ", fantasyName=" + fantasyName + "]";
	}
	
}
