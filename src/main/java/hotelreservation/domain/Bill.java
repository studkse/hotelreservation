package hotelreservation.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Bill {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bill_IDPK;
	@OneToOne
	@JoinColumn(name = "reservation_IDFK", referencedColumnName = "reservation_IDPK")
	private Reservation reservation_IDFK;
	private double charge;
	/* Constructors */
	public Bill() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Bill(Reservation reservation_IDFK) {
		super();
		this.reservation_IDFK = reservation_IDFK;
	}
	/* Getters and Setters */
	public Long getBill_IDPK() {
		return bill_IDPK;
	}
	public void setBill_IDPK(Long bill_IDPK) {
		this.bill_IDPK = bill_IDPK;
	}
	public Reservation getReservation_IDFK() {
		return reservation_IDFK;
	}
	public void setReservation_IDFK(Reservation reservation_IDFK) {
		this.reservation_IDFK = reservation_IDFK;
	}
	public void setCharge(double charge) {
		this.charge = charge;
	}
	public double getCharge() {
		return reservation_IDFK.giveCost();
	}
	/* Methods (Additional) */
	@Override
	public String toString() {
		return "Bill [bill_IDPK=" + bill_IDPK + ", charge=" + charge + "]";
	}
	
}
