package hotelreservation.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Customer extends PersonAccount{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long customer_IDPK;
	@ManyToOne
	@JoinColumn(name = "hotel_IDFK", referencedColumnName = "hotel_IDPK")
	private Hotel hotel_IDFK;
	
	/* Constructors */
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	/* Getters and Setters */
	public Long getCustomer_IDPK() {
		return customer_IDPK;
	}

	public void setCustomer_IDPK(Long customer_IDPK) {
		this.customer_IDPK = customer_IDPK;
	}
	
	public Hotel getHotel_IDFK() {
		return hotel_IDFK;
	}
	public void setHotel_IDFK(Hotel hotel_IDFK) {
		this.hotel_IDFK = hotel_IDFK;
	}
	/* Methods (Additional) */
	@Override
	public String toString() {
		return "Customer [customer_IDPK=" + customer_IDPK + ", getFname()=" + getFname() + ", getLname()=" + getLname()
				+ ", getEmail()=" + getEmail() + ", getPw()=" + getPw() + ", getTel()=" + getTel() + "]";
	}
	
}
