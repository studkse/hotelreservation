package hotelreservation.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "adresses")
public class Adress implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long adress_IDPK;
	@Size(max = 100)
	private String street;
	private int houseNr;
	@Size(max = 100)
	private String city;
	@Size(max = 100)
	private String province;
	@Column(name = "postal_code")
	private int postalCode;
	@Size(max = 100)
	private String country;
	
	/* Constructors */
	public Adress() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Adress(String street, int houseNr, String city, String province, int postalCode,
			String country) {
		super();
		this.street = street;
		this.houseNr = houseNr;
		this.city = city;
		this.province = province;
		this.postalCode = postalCode;
		this.country = country;
	}

	/* Getters and Setters */
	public Long getAdress_IDPK() {
		return adress_IDPK;
	}

	public void setAdress_IDPK(Long adress_IDPK) {
		this.adress_IDPK = adress_IDPK;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getHouseNr() {
		return houseNr;
	}

	public void setHouseNr(int houseNr) {
		this.houseNr = houseNr;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public int getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	/* Methods (Additional) */
	@Override
	public String toString() {
		return "Adress [adress_IDPK=" + adress_IDPK + ", street=" + street + ", houseNr=" + houseNr + ", city=" + city
				+ ", province=" + province + ", postalCode=" + postalCode + ", country=" + country + "]";
	}
	
	
}
