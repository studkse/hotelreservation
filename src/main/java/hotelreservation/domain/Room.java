package hotelreservation.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Room {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long room_IDPK;
	private String type;
	private double costPerDay;
	@ManyToOne
	@JoinColumn(name = "hotel_IDFK", referencedColumnName = "hotel_IDPK")
	private Hotel hotel_IDFK;
	/* Constructors */
	public Room() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Room(String type, double costPerDay) {
		super();
		this.type = type;
		this.costPerDay = costPerDay;
	}
	/* Getters and Setters */
	public Long getRoom_IDPK() {
		return room_IDPK;
	}
	public void setRoom_IDPK(Long room_IDPK) {
		this.room_IDPK = room_IDPK;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getCostPerDay() {
		return costPerDay;
	}
	public void setCostPerDay(double costPerDay) {
		this.costPerDay = costPerDay;
	}
	/* Methods (Additional) */
	@Override
	public String toString() {
		return "Room [room_IDPK=" + room_IDPK + ", type=" + type + ", costPerDay=" + costPerDay + "]";
	}
	
}
