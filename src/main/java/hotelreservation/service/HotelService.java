package hotelreservation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hotelreservation.domain.Hotel;
import hotelreservation.repository.HotelRepository;

@Service
public class HotelService {
	
	@Autowired
	private HotelRepository hotelRepository;

	public HotelService(HotelRepository hotelRepository) {
		this.hotelRepository = hotelRepository;
	}

	public Hotel createHotel(Hotel hotel) {
		return hotelRepository.saveAndFlush(hotel);
	}

//    public Entry createEntryById(Long id) {
//        return entryRepository.findById(id);
////        		.orElseThrow();
//    }

	public List<Hotel> findAllHotels() {
		return hotelRepository.findAll();
	}

	public Hotel findHotelById(Long id) {
		return hotelRepository.findById(id).get();
	}

	public Hotel changeHotelById(Hotel newHotel, Long id) {
		return hotelRepository.findById(id).map(hotel -> {
			hotel.setFantasyName(newHotel.getFantasyName());
			hotel.setAdress_IDFK(newHotel.getAdress_IDFK());
			hotel.setCustomers(newHotel.getCustomers());
			hotel.setWorkers(newHotel.getWorkers());
			hotel.setRooms(newHotel.getRooms());
			return hotelRepository.save(hotel);
		}).orElseGet(() -> {
			newHotel.setHotel_IDPK(id);
			return hotelRepository.save(newHotel);
		});

	}

	public void removeHotelById(Long id) {
		hotelRepository.deleteById(id);
	}
}
